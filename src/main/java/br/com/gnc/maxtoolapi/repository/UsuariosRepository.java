package br.com.gnc.maxtoolapi.repository;

import br.com.gnc.maxtoolapi.model.Usuarios;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuariosRepository extends JpaRepository<Usuarios, Integer> {

}

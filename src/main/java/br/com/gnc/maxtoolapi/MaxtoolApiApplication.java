package br.com.gnc.maxtoolapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MaxtoolApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(MaxtoolApiApplication.class, args);
    }

}

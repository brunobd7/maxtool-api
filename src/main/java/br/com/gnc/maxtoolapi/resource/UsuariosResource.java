package br.com.gnc.maxtoolapi.resource;

import br.com.gnc.maxtoolapi.model.Usuarios;
import br.com.gnc.maxtoolapi.repository.UsuariosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/usuarios")
public class UsuariosResource {

    @Autowired
    private UsuariosRepository usuariosRepository;

    @GetMapping //MAPEAMENTO PARA A URL REQUEST
    public List<Usuarios> listar(){
        return usuariosRepository.findAll();
    }

    @PostMapping
//    @ResponseStatus(HttpStatus.CREATED) // CODE 201 HTTP CRIAÇÃO
    public ResponseEntity<Usuarios> salvar(@RequestBody Usuarios usuario, HttpServletResponse response){ //RQUESTE BODY CONVERTE BODY NA ENTITY

        Usuarios novoUsuario = usuariosRepository.save(usuario);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/usuarios")
                .buildAndExpand(novoUsuario.getId()).toUri();

        response.setHeader("Location", uri.toASCIIString());

        return ResponseEntity.created(uri).body(novoUsuario); //RETORNA O USUARIO CRIADO
    }

    @GetMapping("/{id}")
    public ResponseEntity<Usuarios> consultarPorId(@PathVariable Integer id, HttpServletResponse response){
        Usuarios usuarioFiltrado = usuariosRepository.findById(id).orElse(null); //SUBSTITUIDO FINDONE() A APARTIR DO SPRING DATA JPA 2.0
        return usuarioFiltrado == null ? ResponseEntity.notFound().build() :  ResponseEntity.ok().body(usuarioFiltrado);
    }
}

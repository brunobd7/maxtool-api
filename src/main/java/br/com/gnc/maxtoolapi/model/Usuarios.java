package br.com.gnc.maxtoolapi.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name="usuarios")
public class Usuarios {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String nome;

    private String login;

    private String senha;

    private String tipo;

    private Integer perfil;

    private String licencas;

    private Integer idLicenca;


    //GETS
    public Integer getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getLogin() {
        return login;
    }

    public String getSenha() {
        return senha;
    }

    public String getTipo() {
        return tipo;
    }

    public Integer getPerfil() {
        return perfil;
    }

    public String getLicencas() {
        return licencas;
    }

    public Integer getIdLicenca() {
        return idLicenca;
    }

    //SETS
    public void setId(Integer id) {
        this.id = id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setPerfil(Integer perfil) {
        this.perfil = perfil;
    }

    public void setLicencas(String lincencas) {
        this.licencas = lincencas;
    }

    public void setIdLicenca(Integer idLicenca) {
        this.idLicenca = idLicenca;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Usuarios usuarios = (Usuarios) o;
        return id.equals(usuarios.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

create table usuarios
(
    id         serial not null
        constraint pk_usuarios
            primary key,
    nome       varchar(30),
    login      varchar(80),
    senha      varchar(40),
    email      varchar(60),
    tipo       varchar(1),
    perfil     integer,
    licencas   varchar(600),
    id_licenca integer
);

alter table usuarios
    owner to postgres;

--INSERTS INICIAIS
INSERT INTO public.usuarios (id, nome, login, senha, email, tipo, perfil, licencas, id_licenca) VALUES (889, 'Administrador', '', null, null, 'P', 0, '182', 182);
INSERT INTO public.usuarios (id, nome, login, senha, email, tipo, perfil, licencas, id_licenca) VALUES (890, 'João Alves', 'joaoalves@maxtool.com.br', '8C83770335936419C568CA4B60E2967C', null, 'U', 889, '182', 182);
INSERT INTO public.usuarios (id, nome, login, senha, email, tipo, perfil, licencas, id_licenca) VALUES (891, 'Maria', 'mariapaula@maxtool.com.br', '202CB962AC59075B964B07152D234B70', 'mariapaula@maxtool.com.br', 'U', 897, '182', 182);
INSERT INTO public.usuarios (id, nome, login, senha, email, tipo, perfil, licencas, id_licenca) VALUES (893, 'Antonio', 'antoniosouza@maxtool.com.br', '202CB962AC59075B964B07152D234B70', null, 'U', 889, '182', 182);
INSERT INTO public.usuarios (id, nome, login, senha, email, tipo, perfil, licencas, id_licenca) VALUES (898, 'Fianceiro', '', null, null, 'P', 0, '182', 182);
INSERT INTO public.usuarios (id, nome, login, senha, email, tipo, perfil, licencas, id_licenca) VALUES (899, 'Gestor de departamento', '', null, null, 'P', 0, '182', 182);
INSERT INTO public.usuarios (id, nome, login, senha, email, tipo, perfil, licencas, id_licenca) VALUES (900, 'Ranis', 'ranisj@maxtool.com.br', '8C83770335936419C568CA4B60E2967C', null, 'U', 990, '182', 182);
INSERT INTO public.usuarios (id, nome, login, senha, email, tipo, perfil, licencas, id_licenca) VALUES (924, 'Pedro', 'pedro@maxtool.com.br', '8C83770335936419C568CA4B60E2967C', null, 'U', 889, '182', 182);
INSERT INTO public.usuarios (id, nome, login, senha, email, tipo, perfil, licencas, id_licenca) VALUES (897, 'Executor', '', null, null, 'P', 0, '182', 182);
INSERT INTO public.usuarios (id, nome, login, senha, email, tipo, perfil, licencas, id_licenca) VALUES (948, 'Flávio', 'flavio@maxtool.com.br', '8C83770335936419C568CA4B60E2967C', 'flavio@maxtool.com.br', 'U', 889, '182', 182);
INSERT INTO public.usuarios (id, nome, login, senha, email, tipo, perfil, licencas, id_licenca) VALUES (990, 'teste', '', null, null, 'P', 0, '182', 182);
INSERT INTO public.usuarios (id, nome, login, senha, email, tipo, perfil, licencas, id_licenca) VALUES (1052, 'Lucas', 'lucas@maxtool.com.br', '8C83770335936419C568CA4B60E2967C', null, 'U', 889, '182', 182);
INSERT INTO public.usuarios (id, nome, login, senha, email, tipo, perfil, licencas, id_licenca) VALUES (1053, 'Bruno', 'bruno@maxtool.com.br', '8C83770335936419C568CA4B60E2967C', null, 'U', 889, '182', 182);
INSERT INTO public.usuarios (id, nome, login, senha, email, tipo, perfil, licencas, id_licenca) VALUES (1054, 'Rodrigo', 'rodrigo@maxtool.com.br', '8C83770335936419C568CA4B60E2967C', null, 'U', 889, '182', 182);
INSERT INTO public.usuarios (id, nome, login, senha, email, tipo, perfil, licencas, id_licenca) VALUES (1055, 'Lucas', 'a@a', '8C83770335936419C568CA4B60E2967C', 'a@a', 'U', 0, '182', 182);
INSERT INTO public.usuarios (id, nome, login, senha, email, tipo, perfil, licencas, id_licenca) VALUES (1217, 'Teste', '', null, null, 'P', 0, '182', 182);
INSERT INTO public.usuarios (id, nome, login, senha, email, tipo, perfil, licencas, id_licenca) VALUES (894, 'Kevinn Barbosa', 'kevinnbarbosa@maxtool.com.br', '202CB962AC59075B964B07152D234B70', null, 'U', 889, '182', 182);
INSERT INTO public.usuarios (id, nome, login, senha, email, tipo, perfil, licencas, id_licenca) VALUES (1221, 'José', 'maxtool', '8C83770335936419C568CA4B60E2967C', null, 'U', 897, '182', 182);
